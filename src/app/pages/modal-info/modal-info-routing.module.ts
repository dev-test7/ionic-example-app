import { NgModule } from "@angular/core";

import { ModalInfoPage } from "./modal-info.page";
import { ComponentsModule } from "../../components/components.module";
import { FormsModule } from "@angular/forms";
import { IonicModule } from "@ionic/angular";

@NgModule({
  imports: [ComponentsModule, FormsModule, IonicModule],
  exports: []
})
export class ModalInfoPageRoutingModule {}
