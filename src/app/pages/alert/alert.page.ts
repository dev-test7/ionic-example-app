import { Component, OnInit, Input } from "@angular/core";
import { AlertController } from "@ionic/angular";

@Component({
  selector: "app-alert",
  templateUrl: "./alert.page.html",
  styleUrls: ["./alert.page.scss"]
})
export class AlertPage implements OnInit {
  pageTitle: string = "Alert";

  constructor(public alertController: AlertController) {}

  async presentAlert() {
    const alert = await this.alertController.create({
      header: "Alert",
      subHeader: "Subtitle",
      message: "This is an alert message.",
      buttons: [
        {
          text: "OK",
          cssClass: "primary",
          handler: blah => {
            console.log("Confirm Cancel: ad");
          }
        },
        {
          text: "Cancel",
          role: "cancel",
          cssClass: "secondary",
          handler: blah => {
            console.log("Confirm Cancel: blah");
          }
        }
      ]
    });

    await alert.present();
  }

  async presentAlertPrompt() {
    const alert = await this.alertController.create({
      header: "Prompt!",
      inputs: [
        {
          name: "page_title",
          type: "text",
          placeholder: "Titulo Página"
        }
      ],
      buttons: [
        {
          text: "Cancel",
          role: "cancel",
          cssClass: "secondary",
          handler: () => {
            console.log("Confirm Cancel");
          }
        },
        {
          text: "Ok",
          handler: data => {
            this.pageTitle = data.page_title;
            console.log(Input);
          }
        }
      ]
    });

    await alert.present();
  }

  ngOnInit() {}
}
